# BDD Amazon tests
Hello!

This is the demo of my skills in creating Behavior-Driven Testing from a scratch, using #C programming language, SpecFlow framework and PageObject pattern.

## Description
There are 10 functional automation tets of the Amazon.com website, 2 tests per one feature:
- Basket feature;
- Filters feature;
- LogIn feature;
- Search feature;
- SingIn feature.


## License
Use only to learn about my personal testing skills.
