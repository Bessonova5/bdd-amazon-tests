﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Anna_Bessonova_Kyiv_HT5
{
    public class BasketPage:BasePage
    {
        [FindsBy(How = How.Id, Using = "a-autoid-0-announce")]
        public IWebElement SignInButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'a-row')]/h2")]
        public IWebElement BasketMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@id='nav-cart-count']")]
        public IWebElement BasketCount { get; set; }
        public BasketPage(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public string GetEmptyBasketMessage()
        {
            WaitForElement(10, SignInButton);
            return BasketMessage.Text;
        }

        public string GetBasketIconCount()
        {
            WaitForElement(10, SignInButton);
            return BasketCount.Text;
        }

    }
}
