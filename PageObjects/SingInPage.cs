﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Anna_Bessonova_Kyiv_HT5
{
    public class SingInPage:BasePage
    {
        #region Elements
        [FindsBy(How = How.XPath, Using = "//input[@type='email']")]
        public IWebElement EmailInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@type='submit']")]
        public IWebElement ContinueButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='a-list-item']")]
        public IWebElement ErrorMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='auth-email-missing-alert']//div[@class='a-alert-content']")]
        public IWebElement EmailMissingAlert { get; set; }

        [FindsBy(How = How.Id, Using = "createAccountSubmit")]
        public IWebElement CreateAccountButton { get; set; }

        #endregion
        public SingInPage(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void InsernEmail(string email) => WaitForElement(15, EmailInput).SendKeys(email);            
        
        public void ContinueButtonClick() => ContinueButton.Click();

        public string GetErrorText() => ErrorMessage.Text;

        public string GetAlertText() => EmailMissingAlert.Text;

        public RegistrationPage CreateAccountButtonClick()
        {
            CreateAccountButton.Click();
            return new RegistrationPage(driver);
        }
        
    }
}
