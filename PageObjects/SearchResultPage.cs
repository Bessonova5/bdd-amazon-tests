﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Anna_Bessonova_Kyiv_HT5
{
    public class SearchResultPage:BasePage
    {
        #region Elements
        [FindsBy(How = How.XPath, Using = "//div[@cel_widget_id='MAIN-SEARCH_RESULTS-1']//h2")]
        public IWebElement FirstResult { get; set; }

        #endregion
        public SearchResultPage(IWebDriver driver): base(driver)
        {
            PageFactory.InitElements(driver, this);
        }
        public string GetFirstResultText()
        {
            WaitForElement(10, FirstResult);
            return FirstResult.Text;
        }


    }
}
