﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Interactions;

namespace Anna_Bessonova_Kyiv_HT5
{
    public class RegistrationPage:BasePage 
    {
        #region Elements

        [FindsBy(How = How.XPath, Using = "//input[@type='email']")]
        public IWebElement EmailInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='ap_password']")]
        public IWebElement PasswordInput { get; set; }        

        [FindsBy(How = How.XPath, Using = "//div[@id='auth-email-invalid-claim-alert']/div/div")]
        public IWebElement EmailAlertMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='auth-password-invalid-password-alert']/div/div")]
        public IWebElement PasswordAlertMessage { get; set; }
        #endregion

        public RegistrationPage(IWebDriver driver) : base(driver)
        {
                PageFactory.InitElements(driver, this);
        }        
        public void SendEmail(string email) => EmailInput.SendKeys(email + Keys.Enter);

        public void SendPassword(string password) => PasswordInput.SendKeys(password + Keys.Enter);

        public string GetEmailAlertText() =>  WaitForElement(10, EmailAlertMessage).Text;

        public string GetPasswordAlertText() => WaitForElement(10, PasswordAlertMessage).Text;
    }
}
