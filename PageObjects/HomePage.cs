﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Anna_Bessonova_Kyiv_HT5
{
    public class HomePage:BasePage
    {
        #region Elements

        [FindsBy(How = How.Id, Using = "nav-link-accountList")]
        public IWebElement SingInButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@aria-label='Video Games']")]
        public IWebElement VideoGamesCategory { get; set; }

        [FindsBy(How = How.Id, Using = "twotabsearchtextbox")]
        public IWebElement SearchBox { get; set; }

        [FindsBy(How = How.Id, Using = "nav-search-submit-button")]
        public IWebElement SearchButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@id='nav-cart']")]
        public IWebElement BasketButton { get; set; }
                
        #endregion

        public By HomePageBanner { get; } = By.XPath("//div[@id='desktop-banner']");

        public HomePage (IWebDriver driver): base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public SingInPage ClickSingInButton()
        {
            SingInButton.Click();
            return new SingInPage(driver);
        }

        public VideoGamesPage ClickVideoGamesCategory()
        {
            VideoGamesCategory.Click();
            return new VideoGamesPage(driver);
        }

        public void SendSearchText(string ceraVe) => SearchBox.SendKeys(ceraVe);
        public SearchResultPage SearchButtonClick()
        {
            SearchButton.Click();
            return new SearchResultPage(driver);
        }
        
        public bool IsHomePageBannerPresent() => WaitForElementToBePresent(10, HomePageBanner).Displayed;

        public BasketPage ClickOnBasketButton()
        {
            BasketButton.Click();
            return new BasketPage(driver);
        }
    }
}
