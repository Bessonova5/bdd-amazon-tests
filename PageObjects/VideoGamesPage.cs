﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Interactions;

namespace Anna_Bessonova_Kyiv_HT5
{
    public class VideoGamesPage:BasePage
    {
        #region Elements
        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'fst-h1-st pageBanner')]//b")]
        public IWebElement VideoGamesHeader { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'Xbox')]/../div//input")]
        public IWebElement XboxCheckbox { get; set; }
        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'Play')]/../div//input")]
        public IWebElement PlayStationCheckbox { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='sg-row']//div[@class='a-section a-spacing-small a-spacing-top-small']")]
        public IList <IWebElement> Products { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='$25 to $35']/..")]
        public IWebElement PriceRange { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@cel_widget_id='MAIN-SEARCH_RESULTS-2']//span[@class='a-price-whole']")]
        public IWebElement ProductPrice { get; set; }
        #endregion

        public VideoGamesPage(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }
        public void SelectXboxCheckbox()
        {
            Actions act = new Actions(driver);
            act.MoveToElement(XboxCheckbox).Click().Perform();
        }
        public void SelectPlayStationCheckbox()
        {
            Actions act = new Actions(driver);
            act.MoveToElement(PlayStationCheckbox).Click().Perform();
        }
        public void SelectPriceRange() => PriceRange.Click();
        public string GetProductPrice()
        {            
            WaitForElement(20, ProductPrice); 
            return ProductPrice.Text;
        }

        public IEnumerable<string> GetProductsText()
        {
           return Products.Select(x => x.Text);
        }
    }
}
