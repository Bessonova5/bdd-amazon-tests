﻿Feature: SignIn

As a tester,
I want to test sign in functionality
to check that it works correct with negative data.

	@feature3
Scenario: Wrong Email Format
	Given User goes to base page
	And Click registration button
	And Click on create amazon account
	When User enters invalid email 'abcdef'
	Then Invalid email alert occurs

	@feature3
Scenario: Wrong Password Format
	Given User goes to base page
	And Click registration button
	And Click on create amazon account
	When User enters invalid password '1111'
	Then Invalid password alert occurs