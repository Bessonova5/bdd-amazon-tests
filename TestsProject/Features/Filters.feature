﻿Feature: Filters

As a tester,
I want to test filters functionality
To check that it works correctly

@feature2
Scenario: Featured Brands Filter
	Given User goes to main page
	And  Open video game category
	When User select Xbox checkbox
	And  User select PlayStation checkbox
	Then Products description contains 'Xbox' or 'PlayStation' 

@feature2
Scenario: Price Filter
	Given User goes to main page
	And  Open video game category
	When User selects price range
	Then Product price is in the selected range 