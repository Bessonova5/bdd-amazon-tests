﻿Feature: Search

As a tester,
I want to test search functionality
to check that it works correct with positive and negative data.

@feature4
Scenario: Search Of Specific Product
	Given User goes to site page
	When User enter product name 'CeraVe' into search
	And  User click on search button
	Then First product is corresponding to request 'CeraVe'

@feature4
Scenario: Empty String Search
	Given User goes to site page
	When  User click on search button
	Then Home page is opened
