﻿Feature: LogIn

As a tester,
I want to test log in functionality
to check that it works correct with negative data.

@feature1
Scenario: Log In Unregisted Email
	Given User goes to home page
	And Click sign in button
	When  User enter unregistered email 'anyaplahturya@gmail.com'
	And   User click continue button
	Then  Error message 'We cannot find an account with that email address' occurs 

@feature1
Scenario: Log In Without Email
Given User goes to home page
And   Click sign in button
When User click continue button
Then Alert message 'Enter your email or mobile phone number' occurs