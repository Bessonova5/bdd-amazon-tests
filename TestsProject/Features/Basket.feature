﻿Feature: Basket

As a tester,
I want to test basket functionality
To check that it's empty when user have just opened home page

@feature5
Scenario: Basket Is Empty
	Given User goes to main site page
	When User click on basker icon
	Then Empty basket text 'Your Amazon Cart is empty' is visible

@feature5
Scenario: Basket Icon Has Zero Item
	Given User goes to main site page
	When User click on basker icon
	Then Basket icon contains number '0'