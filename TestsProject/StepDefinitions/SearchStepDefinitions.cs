using OpenQA.Selenium.Chrome;
using Anna_Bessonova_Kyiv_HT5;
using OpenQA.Selenium;
using NUnit.Framework;

namespace TestsProject.StepDefinitions
{
    [Binding]
    public class SearchStepDefinitions
    {
        public IWebDriver Driver { get; set; }
        HomePage HomePage { get; set; }
        SearchResultPage SearchResultPage { get; set; }

        [BeforeScenario("@feature4")]
        public void SetUp()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("https://www.amazon.com/");
        }

        [Given(@"User goes to site page")]
        public void GivenUserGoesToSitePage() => HomePage = new HomePage(Driver);

        [When(@"User enter product name '([^']*)' into search")]
        public void WhenUserEnterProductNameIntoSearch(string ceraVe) => HomePage.SendSearchText(ceraVe);

        [When(@"User click on search button")]
        public void WhenUserClickOnSearchButton() => SearchResultPage = HomePage.SearchButtonClick();

        [Then(@"First product is corresponding to request '([^']*)'")]
        public void ThenFirstProductIsCorrespondingToRequest(string ceraVe)
        {            
           Assert.IsTrue(SearchResultPage.GetFirstResultText().Contains(ceraVe));
        }

        [Then(@"Home page is opened")]
        public void ThenHomePageIsOpened()
        {
            Assert.IsTrue(HomePage.IsHomePageBannerPresent());
        }

        [AfterScenario("@feature4")]
        public void TearDown()
        {
            Driver.Close();
            Driver.Quit();
        }
    }
}
