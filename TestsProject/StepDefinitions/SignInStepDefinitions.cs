using OpenQA.Selenium.Chrome;
using Anna_Bessonova_Kyiv_HT5;
using OpenQA.Selenium;
using NUnit.Framework;


namespace TestsProject.StepDefinitions
{
    [Binding]
    public class SignInStepDefinitions
    {
        public IWebDriver Driver { get; set; }
        HomePage HomePage { get; set; }
        SingInPage SingInPage { get; set; }
        RegistrationPage RegistrationPage { get; set; }

        [BeforeScenario("@feature3")]
        public void SetUp()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("https://www.amazon.com/");
        }

        [Given(@"User goes to base page")]
        public void GivenUserGoesToBasePage() => HomePage = new HomePage(Driver);

        [Given(@"Click registration button")]
        public void GivenClickRegistrationButton() => SingInPage = HomePage.ClickSingInButton();

        [Given(@"Click on create amazon account")]
        public void GivenClickOnCreateAmazonAccount() => RegistrationPage = SingInPage.CreateAccountButtonClick();

        [When(@"User enters invalid email '([^']*)'")]
        public void WhenUserEntersInvalidEmail(string email) => RegistrationPage.SendEmail(email);

        [Then(@"Invalid email alert occurs")]
        public void ThenInvalidEmailAlertOccurs()
        {
            Assert.AreEqual("Wrong or Invalid email address or mobile phone number. Please correct and try again.",
                RegistrationPage.GetEmailAlertText());
        }

        [When(@"User enters invalid password '([^']*)'")]
        public void WhenUserEntersInvalidPassword(string passsword) => RegistrationPage.SendPassword(passsword);

        [Then(@"Invalid password alert occurs")]
        public void ThenInvalidPasswordAlertOccurs()
        {
            Assert.AreEqual("Minimum 6 characters required", RegistrationPage.GetPasswordAlertText());
        }
        [AfterScenario("@feature3")]
        public void TearDown()
        {
            Driver.Close();
            Driver.Quit();
        }
    }
}
