using OpenQA.Selenium.Chrome;
using Anna_Bessonova_Kyiv_HT5;
using OpenQA.Selenium;
using NUnit.Framework;

namespace TestsProject.StepDefinitions
{
    [Binding]
    public class FiltersStepDefinitions
    {
        public IWebDriver Driver { get; set; }
        HomePage HomePage { get; set; }
        VideoGamesPage VideoGamesPage { get; set; }

        [BeforeScenario("@feature2")]
        public void SetUp()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("https://www.amazon.com/");
        }

        [Given(@"User goes to main page")]
        public void GivenUsergoToHomePage() => HomePage = new HomePage(Driver);

        [Given(@"Open video game category")]
        public void GivenOpenVideoGameCategory() => VideoGamesPage = HomePage.ClickVideoGamesCategory();

        [When(@"User select Xbox checkbox")]
        public void WhenUserSelectXboxCheckbox() => VideoGamesPage.SelectXboxCheckbox();

        [When(@"User select PlayStation checkbox")]
        public void WhenUserSelectPlayStationCheckbox() => VideoGamesPage.SelectPlayStationCheckbox();

        [Then(@"Products description contains '([^']*)' or '([^']*)'")]
        public void ThenProductsDescriptionContainsOr(string xbox, string playStation)
        {
            Assert.IsTrue(VideoGamesPage.GetProductsText()
                .Select(x => x.Contains(xbox) || x.Contains(playStation))
                .Count().Equals(16));
        }

        [When(@"User selects price range")]
        public void WhenUserSelectsPriceRange() => VideoGamesPage.SelectPriceRange();

        [Then(@"Product price is in the selected range")]
        public void ThenProductPriceIsInTheSelectedRange()
        {
            var price = int.Parse(VideoGamesPage.GetProductPrice());
            Assert.IsTrue(price >= 25 && price <= 35);
        }

        [AfterScenario("@feature2")]
        public void TearDown()
        {
            Driver.Close();
            Driver.Quit();
        }
    }
}
