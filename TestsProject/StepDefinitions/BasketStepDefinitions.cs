using OpenQA.Selenium.Chrome;
using Anna_Bessonova_Kyiv_HT5;
using OpenQA.Selenium;
using NUnit.Framework;

namespace TestsProject.StepDefinitions
{
    [Binding]
    public class BasketStepDefinitions
    {
        public IWebDriver Driver { get; set; }
        HomePage HomePage { get; set; }

        BasketPage BasketPage { get; set; }

        [BeforeScenario("@feature5")]
        public void SetUp()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("https://www.amazon.com/");
        }
        [Given(@"User goes to main site page")]
        public void GivenUserGoesToMainSitePage() => HomePage = new HomePage(Driver);

        [When(@"User click on basker icon")]
        public void WhenUserClickOnBaskerIcon() => BasketPage = HomePage.ClickOnBasketButton();

        [Then(@"Empty basket text '([^']*)' is visible")]
        public void ThenEmptyBasketTextIsVisible(string message)
        {
            Assert.AreEqual(message, BasketPage.GetEmptyBasketMessage());
        }

        [Then(@"Basket icon contains number '([^']*)'")]
        public void ThenBasketIconContainsNumber(string zero)
        {
            Assert.AreEqual(zero, BasketPage.GetBasketIconCount());
        }

        [AfterScenario("@feature5")]
        public void TearDown()
        {
            Driver.Close();
            Driver.Quit();
        }
    }
}
