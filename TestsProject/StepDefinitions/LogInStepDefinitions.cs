using OpenQA.Selenium.Chrome;
using Anna_Bessonova_Kyiv_HT5;
using OpenQA.Selenium;
using NUnit.Framework;

namespace TestsProject.StepDefinitions
{
    [Binding]
    public class LogInStepDefinitions
    {
        public IWebDriver Driver { get; set; }
        HomePage HomePage { get; set; }
        SingInPage SingInPage { get; set; }

        [BeforeScenario("@feature1")]
        public void SetUp()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("https://www.amazon.com/");
        }

        [Given(@"User goes to home page")]
        public void GivenUserGoToHomePage() => HomePage = new HomePage(Driver);

        [Given(@"Click sign in button")]
        public void GivenClickSignInButton() => SingInPage = HomePage.ClickSingInButton();

        [When(@"User enter unregistered email '([^']*)'")]
        public void WhenUserEnterUnregisteredEmail(string email) => SingInPage.InsernEmail(email);

        [Then(@"Error message '([^']*)' occurs")]
        public void ThenErrorMessageOccurs(string message) =>  Assert.AreEqual(message,
                 SingInPage.GetErrorText());

        [When(@"User click continue button")]
        public void WhenUserClickContinueButton() => SingInPage.ContinueButtonClick();

        [Then(@"Alert message '([^']*)' occurs")]
        public void ThenAlertMessageOccurs(string message) => Assert.AreEqual(message,
                SingInPage.GetAlertText());

        [AfterScenario("@feature1")]
        public void TearDown()
        {
            Driver.Close();
            Driver.Quit();
        }
    }
}
